import datetime
import unittest
from hmacgenerator import HmacGenerator

class TestHmacGenerator(unittest.TestCase):
    def setUp(self):
        self.generator = HmacGenerator()

    def testGenerationSingle(self):
        key = "abc123"
        data = "some big, long data string or something"

        timeStart = datetime.datetime.now()
        result = self.generator.generate(key, data)
        duration = datetime.datetime.now() - timeStart

        print "\nSingle generate completed: %s ms\n" % duration.microseconds

    def testGenerationMultiple(self):
        key = open("../sample-data/key", "r").read()
        data = open("../sample-data/data.json", "r").read()

        timeStart = datetime.datetime.now()
        iterations = 100000
        result = ""
        nextResult = ""
        for index in range(0, iterations):
            nextResult = self.generator.generate(key, data)
            if index == 0:
                result = nextResult
            else:
                self.assertEqual(result, nextResult)

        duration = datetime.datetime.now() - timeStart
        print "\nMulti-generate completed (%d iterations): %s ms\n" % (iterations, duration.microseconds)

if __name__ == '__main__':
    unittest.main()
