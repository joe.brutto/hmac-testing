import hmac
import hashlib

class HmacGenerator(object):
  def generate(self, key, data):
      return hmac.new(key, data, hashlib.sha256).hexdigest()
