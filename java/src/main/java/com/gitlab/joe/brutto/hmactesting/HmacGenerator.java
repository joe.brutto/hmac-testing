package com.gitlab.joe.brutto.hmactesting;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HmacGenerator {
  /**
   * Algorithm to use for HMAC-SHA256 generation.
   */
  public static final String ALGORITHM = "HmacSHA256";

  /**
   * Default encoding to use for strings.
   */
  public static final String DEFAULT_ENCODING = "UTF-8";

  /**
   * Generates an HMAC for the given data.
   * @param key the key to use to generate the HMAC
   * @param data the data that we want to generate the actual HMAC for
   * @return the HMAC result as a hex string
   * @throws Exception if anyting goes wrong (don't do this in real code, it's bad practice)
   */
  public static String generate(String key, String data) throws Exception {
    // basic setup
    Mac sha256Hmac = Mac.getInstance(ALGORITHM);

    byte[] keyBytes = key.getBytes(DEFAULT_ENCODING);
    SecretKeySpec keySpec = new SecretKeySpec(keyBytes, ALGORITHM);
    sha256Hmac.init(keySpec);

    // actual data generation
    byte[] dataBytes = data.getBytes(DEFAULT_ENCODING);
    byte[] hmacBytes = sha256Hmac.doFinal(dataBytes);

    // convert & exit
    return bytesToHex(hmacBytes);
  }

  /**
   * Converts a byte array to hex.
   * @param bytes the byte array to convert
   * @return the converted hex string
   */
  private static String bytesToHex(byte[] bytes) {
    StringBuilder buffer = new StringBuilder();
    for (byte nextByte : bytes) {
      buffer.append(String.format("%02x", nextByte));
    }
    return buffer.toString();
  }
}
