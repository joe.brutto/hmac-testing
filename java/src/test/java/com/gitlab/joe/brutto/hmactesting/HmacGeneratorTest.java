package com.gitlab.joe.brutto.hmactesting;

import java.io.File;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class HmacGeneratorTest {

  private static final int ITERATIONS = 100000;

  @Test
  public void testGenerationSingle() throws Exception {
    String key = "abc123";
    String data = "some big, long data string or something";

    long timeStart = System.currentTimeMillis();
    String result = HmacGenerator.generate(key, data);
    long duration = System.currentTimeMillis() - timeStart;

    Assert.assertNotNull(result);
    System.out.println(String.format("Single generate completed: %s ms", duration));
  }

  @Test(dependsOnMethods = "testGenerationSingle")
  public void testGenerationOfATon() throws Exception {
    String key = IOUtils.toString(new File("../sample-data/key").toURI(), "UTF-8");
    String data = IOUtils.toString(new File("../sample-data/data.json").toURI(), "UTF-8");

    long timeStart = System.currentTimeMillis();
    String result = "", nextResult = "";
    for (int index = 0; index < ITERATIONS; index++) {
      nextResult = HmacGenerator.generate(key, data);
      if (index == 0) {
        result = nextResult;
      } else {
        Assert.assertEquals(result, nextResult);
      }
    }

    long duration = System.currentTimeMillis() - timeStart;
    System.out.println(String.format("Multi-generate completed (%d iterations): %s ms", ITERATIONS, duration));
  }
}
