package main

import (
	"fmt"
	"hmac-samples/generator"
)

func main() {
	err := SimpleExecution()
	if err != nil {
		panic(err)
	}
}

func SimpleExecution() error {
	// quick test using hard-coded data
	secret := "abc123"
	data := "some big, long data string or something"

	// do it
	hexResult, err := generator.Generate([]byte(secret), []byte(data))
	if err == nil {
		fmt.Printf("Secret[%v], Data[%v], HMAC[%v]\n", secret, data, hexResult)
	}

	// return any errors
	return err
}
