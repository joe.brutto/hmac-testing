package generator

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
	"time"
)

const (
	Iterations = 100000
)

func TestGenerate(t *testing.T) {
	// grab our sample key & data
	secret, err := ioutil.ReadFile("../../sample-data/key")
	assert.Nil(t, err, "Unable to read key file")
	assert.NotEmpty(t, secret, "No secret data loaded")

	data, err := ioutil.ReadFile("../../sample-data/data.json")
	assert.Nil(t, err, "Unable to read data file")
	assert.NotEmpty(t, data, "No sample data loaded")

	fmt.Printf("Loaded secret from file: %T (%v)\n", secret, len(secret))
	fmt.Printf("Loaded data from file: %T (%v)\n", data, len(data))

	// single transform
	timeStart := time.Now()
	_, err = Generate(secret, data)
	assert.Nil(t, err, "Unable to generate HMAC")

	duration := time.Since(timeStart)
	fmt.Printf("Single generate completed: %v\n", duration)

	// multi-transform
	timeStart = time.Now()
	last := ""
	for index := 0; index < Iterations; index++ {
		newest, err := Generate(secret, data)
		if err != nil {
			break
		}

		if index != 0 {
			assert.Equal(t, last, newest, "HMAC mismatch")
		}
		last = newest
	}
	assert.Nil(t, err, "Unable to generate HMAC")
	duration = time.Since(timeStart)

	fmt.Printf("Multi-generate completed (%d iterations): %v\n", Iterations, duration)
}
