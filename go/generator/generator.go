package generator

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

func Generate(key []byte, data []byte) (string, error) {
	var hexString string

	instance := hmac.New(sha256.New, key)
	_, err := instance.Write(data)
	if err == nil {
		hexString = hex.EncodeToString(instance.Sum(nil))
	}

	return hexString, err
}
