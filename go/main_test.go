package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSimpleExecution(t *testing.T) {
	err := SimpleExecution()
	assert.Nil(t, err, "Unable to execute SimpleExecution()")
}
