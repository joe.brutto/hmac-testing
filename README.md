# HMAC Samples

Simple project with some HMAC-SHA256 calculations in various languages I care about. These are basic examples to illustrate how it's done in a basic sense and to get a feel of how expensive these operations are. I try to do this without a slew of third party libraries so that it's more generic than some other examples I've found out there.

There's some _playground_ aspects to this project so these may not be the most efficient implementations and may be naive in some aspects - **use at your own risk.**

## Some Notes/Assumptions

* Not doing any concurrency here, just straight calculations
* Language-specific optimization under the covers we are not going to try to unravel
* All benchmarking needs to be run in whatever you want to actually run in (local machines, virtual machines, etc.)
* Reports of time come from the machine specified in the details below

## Languages Covered

I'm only covering the languages I mostly use/support in projects, so if your favorite isn't here then add a Pull Request.

* Go
* Java
* Python
* JavaScript
